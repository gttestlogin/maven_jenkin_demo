package example;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class Hello {
	public WebDriver driver;
	Actions act;
	JavascriptExecutor js;
	WebDriverWait wait;

	@Parameters("browser")
	@BeforeTest
	public void setUp(String browserName) {
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\BrowserDrivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}

		else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\BrowserDrivers\\geckodriver.exe");
			driver = new FirefoxDriver();
		}

		else if (browserName.equalsIgnoreCase("Edge")) {
			// set path to Edge.exe
			// System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")
			// +"\\BrowserDrivers\\msedgedriver.exe");
			// create Edge instance
			driver = new EdgeDriver();
		}

		else if (browserName.equalsIgnoreCase("ie")) {
			// set path to Edge.exe
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\BrowserDrivers\\IEDriverServer.exe");
			// create Edge instance
			driver = new InternetExplorerDriver();
		}

		else if (browserName.equalsIgnoreCase("opera")) {
			// set system property, so that we can access opera driver
			// System.setProperty("webdriver.opera.driver", System.getProperty("user.dir") +
			// "\\BrowserDrivers\\operadriver.exe");
			// it will open the opera browser
			OperaOptions options = new OperaOptions();
			options.setBinary(new File("C:\\Users\\LAP12778-local\\AppData\\Local\\Programs\\Opera\\60.0.3255.170_0\\opera.exe"));
			driver = new OperaDriver(options);
		}

		else if (browserName.equalsIgnoreCase("htmlunit")) {
			driver = new HtmlUnitDriver();
		}
		/*
		 * else if (browserName.equalsIgnoreCase("phantomjs")) { File file = new
		 * File(System.getProperty("user.dir") +
		 * "\\BrowserDrivers\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe"); //File file
		 * = new File("C:\\Program Files\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe");
		 * System.setProperty("phantomjs.binary.path", file.getAbsolutePath()); driver =
		 * new
		 * PhantomJSDriver("C:\\Program Files\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe"
		 * ); }
		 */
		else
			System.out.println("This driver is not supported.");

		/*
		 * act = new Actions(driver); js = (JavascriptExecutor) driver; wait = new
		 * WebDriverWait(driver, 5); //driver.manage().window().maximize();
		 * driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		 */
	}

	@AfterTest
	public void tearDown() {
		// driver.quit();
	}

	@Test
	public void test() throws InterruptedException {
		//System.out.println("Start nha!");
		driver.get("http://google.com.vn");
		System.out.println("driver.getTitle(): " + driver.getTitle());

		/*
		 * // Step 1: Go to http://the-internet.herokuapp.com/dynamic_loading/2
		 * driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		 * 
		 * // Step 2: Define an implicit wait (if you set 2 seconds, test will fail) //
		 * Pham vi ap dung: tu luc duoc set la anh huong tat ca cac steps co chua hang
		 * // findElement/findElements // timout mac dinh cho findElement = implicitWait
		 * timeout driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		 * 
		 * // Step 3: Click the Start button WebElement startBtn =
		 * driver.findElement(By.xpath("//div[@id='start']/button")); // Check if
		 * emelement is displayed/visible Assert.assertTrue(startBtn.isDisplayed());
		 * startBtn.click();
		 * 
		 * // Step 4: Wait result text appear WebElement result =
		 * driver.findElement(By.xpath("//div[@id='finish']/h4")); // WebElement result
		 * = // wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
		 * "//div[@id='finish']/h4"))); Assert.assertTrue(result.isDisplayed());
		 * 
		 * // Step 5: Check result text is "Hello World"
		 * Assert.assertTrue(result.getText().equals("Hello World!"));
		 */
	}
}
